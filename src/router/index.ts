import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import Layout from '../views/Layout.vue';
import ContractList from '../views/Contracts/ContractList.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/contracts',
    name: 'Contracts',
    component: Layout,
    children: [{
      path: '/contracts',
      name: 'ContractsList',
      component: ContractList,
    }, {
      path: '/contracts/:id',
      name: 'Single Contract View',
      props: { id: true },
      component: () => import(/* webpackChunkName: "contract" */ '../views/Contracts/ContractForm.vue'),
    }],
  }, {
    path: '/:pathMatch(.*)', redirect: { name: 'ContractsList' },
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
