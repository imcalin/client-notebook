import PrimeVue from 'primevue/config';
import InputText from 'primevue/inputtext';
import Button from 'primevue/button';
import AutoComplete from 'primevue/autocomplete';

import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '@/assets/flags.scss';
import '@/assets/style.scss';

const GlobalComponents = {
  install(Vue: any) {
    Vue.use(PrimeVue);
    Vue.component('Button', Button);
    Vue.component('InputText', InputText);
    Vue.component('AutoComplete', AutoComplete);
  },
};

export default GlobalComponents;
