const globalUtils = {
  generateId() {
    return Math.random().toString(36).substring(7);
  },
  emailValidation(email: string) {
    const regex = /\S+@\S+\.\S+/;
    return regex.test(email);
  },
  validateForm(form: any) {
    let isValid = true;
    let error = '';
    const stringReg = /^[\s]*$/;
    for (const key in form) {
      if (key !== 'prototype') {
        switch (key) {
          case 'email':
            isValid = form[key] && this.emailValidation(form[key]);
            break;
          case 'country':
            isValid = form[key]?.name?.length && form[key]?.code?.length;
            break;
          default:
            isValid = !stringReg.test(form[key]);
            break;
        }
        if (!isValid) {
          error = key;
          break;
        }
      }
    }
    return { isValid, error };
  },
};
export default globalUtils;
