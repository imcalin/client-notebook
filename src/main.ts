import { createApp } from 'vue';

import App from './App.vue';
import GlobalComponents from './plugins/globalComponents';
import router from './router';
import store from './store';

createApp(App)
  .use(store)
  .use(router)
  .use(GlobalComponents)
  .mount('#app');
