import { ContractModel } from '@/models/contract.model';

const contractsStore = {
  state: () => ({
    contracts: localStorage.getItem('contracts') ? JSON.parse(localStorage.getItem('contracts') || '') : [] as any,
  }),
  mutations: {
    addContract(state: any, newContract: ContractModel) {
      state.contracts.push(newContract);
      localStorage.setItem('contracts', JSON.stringify(state.contracts));
    },
    editContract(state: any, contract: ContractModel) {
      const index = state.contracts.findIndex((oldContract: ContractModel) => oldContract.id === contract.id);
      state.contracts[index] = contract;
      localStorage.setItem('contracts', JSON.stringify(state.contracts));
    },
    deleteContract(state: any, id: string) {
      state.contracts = state.contracts.filter((contract: ContractModel) => contract.id !== id);
      localStorage.setItem('contracts', JSON.stringify(state.contracts));
    },
  },
  getters: {
    getContract: (state: any) => (id: string) => state.contracts.find((contract: ContractModel) => contract.id === id),
  },
};

export default contractsStore;
