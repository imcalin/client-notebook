import { createStore } from 'vuex';
import contractsStore from './contractsStore';

const store = createStore({
  modules: {
    contractsModule: contractsStore,
  },
});
export default store;
